FROM nginx

MAINTAINER youki <youkiplus@163.com>

COPY ./dist /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/nginx.conf
